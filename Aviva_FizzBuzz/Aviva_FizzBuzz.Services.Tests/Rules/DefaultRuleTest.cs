﻿//-----------------------------------------------------------------------
// <copyright file="DefaultRuleTest.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Test.Rules
{
    using System;
    using Aviva_FizzBuzz.Services.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test default rules.
    /// </summary>
    [TestFixture]
    public class DefaultRuleTest
    {
        /// <summary>
        /// Mock object for DefaultRule
        /// </summary>
        private Mock<DefaultRule> defaultRule;

        /// <summary>
        /// Setup for all test methods
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.defaultRule = new Mock<DefaultRule>();
        }

        /// <summary>
        /// ModCheck method should return true when number is not divisible by three and five.
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            bool result;
            this.defaultRule.Setup(x => x.ModCheck(1)).Returns(true);

            result = this.defaultRule.Object.ModCheck(1);

            result.Should().Be(true);
        }

        /// <summary>
        /// Print method should return integer value when called.
        /// </summary>
        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;
            this.defaultRule.Setup(x => x.Print(1, It.IsAny<DayOfWeek>())).Returns("1");

            output = this.defaultRule.Object.Print(1, DayOfWeek.Monday);

            output.Should().Be("1");
        }
    }
}
