﻿//-----------------------------------------------------------------------
// <copyright file="FizzRuleTest.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Test.Rules
{
    using System;
    using Aviva_FizzBuzz.Services.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test fizz rules...
    /// </summary>
    [TestFixture]
    public class FizzRuleTest
    {
        /// <summary>
        /// Mock object for FizzRule
        /// </summary>
        private Mock<FizzRule> fizzRule;

        /// <summary>
        /// Setup for all test methods
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.fizzRule = new Mock<FizzRule>();
        }

        /// <summary>
        /// ModCheck method should return true when number is divisible by three.
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByThree()
        {
            bool result;
            this.fizzRule.Setup(m => m.ModCheck(3)).Returns(true);

            result = this.fizzRule.Object.ModCheck(3);

            result.Should().Be(true);
        }

        /// <summary>
        /// Print method should return fizz when called.
        /// </summary>
        [Test]
        public void Print_ShouldReturnFizz_WhenCalled()
        {
            string output;
            this.fizzRule.Setup(m => m.Print(3, It.IsAny<DayOfWeek>())).Returns("fizz");

            output = this.fizzRule.Object.Print(3, DayOfWeek.Monday);

            output.Should().Be("fizz");
        }

        /// <summary>
        /// Print method should return wizz when day of the week is wednesday.
        /// </summary>
        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;
            this.fizzRule.Setup(m => m.Print(3, DayOfWeek.Wednesday)).Returns("wizz");

            output = this.fizzRule.Object.Print(3, DayOfWeek.Wednesday);

            output.Should().Be("wizz"); 
        }
    }
}
