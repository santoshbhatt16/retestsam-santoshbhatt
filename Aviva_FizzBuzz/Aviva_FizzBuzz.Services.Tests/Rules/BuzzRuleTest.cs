﻿//-----------------------------------------------------------------------
// <copyright file="BuzzRuleTest.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Test.Rules
{
    using System;
    using Aviva_FizzBuzz.Services.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test buzz rules...
    /// </summary>
    [TestFixture]
    public class BuzzRuleTest
    {
        /// <summary>
        /// Mock object for BuzzRule
        /// </summary>
        private Mock<BuzzRule> buzzRule;

        /// <summary>
        /// Setup for all test methods
        /// </summary>
        [SetUp]
        public void SetUpTests()
        {
            this.buzzRule = new Mock<BuzzRule>();
        }

        /// <summary>
        /// ModCheck method should return true when number is divisible by five.
        /// </summary>
        [Test]
        public void ModCheck_ShouldReturnTrue_WhenNumberIsDivisibleByFive()
        {
            bool result;
            this.buzzRule.Setup(x => x.ModCheck(5)).Returns(true);

            result = this.buzzRule.Object.ModCheck(5);

            result.Should().Be(true);
        }

        /// <summary>
        /// Print method should return buzz when called.
        /// </summary>
        [Test]
        public void Print_ShouldReturnBuzz_WhenModCheckReturnsTrue()
        {
            string output;
            this.buzzRule.Setup(x => x.Print(5, It.IsAny<DayOfWeek>())).Returns("buzz");

            output = this.buzzRule.Object.Print(5, DayOfWeek.Monday);

            output.Should().Be("buzz");
        }

        /// <summary>
        /// Print method should return wuzz when day of the week is wednesday.
        /// </summary>
        [Test]
        public void Print_ShouldReturnWizz_WhenDayOfWeekIsWednesday()
        {
            string output;
            this.buzzRule.Setup(m => m.Print(5, DayOfWeek.Wednesday)).Returns("wuzz");

            output = this.buzzRule.Object.Print(5, DayOfWeek.Wednesday);

            output.Should().Be("wuzz");
        }
    }
}
