﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzServiceTest.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Tests.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Aviva_FizzBuzz.Services;
    using Aviva_FizzBuzz.Services.Contracts;
    using Aviva_FizzBuzz.Services.Rules;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Test class to test Fizz Buzz Service
    /// </summary>
    [TestFixture]
    public class FizzBuzzServiceTest
    {
        /// <summary>
        /// IRule List
        /// </summary>
        private IList<IRules> rules;

        /// <summary>
        /// Mock object creation for FizzRule class
        /// </summary>
        private Mock<FizzRule> fizzBuzz = new Mock<FizzRule>();

        /// <summary>
        /// Mock object creation for BuzzRule class
        /// </summary>
        private Mock<BuzzRule> buzzRule = new Mock<BuzzRule>();

        /// <summary>
        /// Mock object creation for DefaultRule class
        /// </summary>
        private Mock<DefaultRule> defaultRule = new Mock<DefaultRule>();

        /// <summary>
        /// Object of interface IFizzBuzzService
        /// </summary>
        private IFizzBuzzService service;

        /// <summary>
        /// List to collect values from service class
        /// </summary>
        private List<string> fizzBuzzDataList = null;

        /// <summary>
        /// Initialize objects for each test method calls.
        /// </summary>
        [SetUp]
        public void SetupTests()
        {
            this.rules = new List<IRules>();
            this.rules.Add(this.fizzBuzz.Object);
            this.rules.Add(this.buzzRule.Object);
            this.rules.Add(this.defaultRule.Object);

            this.service = new FizzBuzzService(this.rules);
        }

        /// <summary>
        /// GetFizzBuzzData method should return list count equal to the user entered value
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnDataCount_EqualToUserInput()
        {
            List<string> listFizzBuzzData = this.service.GetFizzBuzzData(15);

            listFizzBuzzData.Count.Should().Be(15);
        }

        /// <summary>
        /// GetFizzBuzzData method returns number if not divisible by three and five
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnIntegerValue_WhenNumberIsNotDivisibleByThreeAndFive()
        {
            this.defaultRule.Setup(m => m.ModCheck(1)).Returns(true);
            this.defaultRule.Setup(m => m.Print(1, It.IsAny<DayOfWeek>())).Returns("1");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(1);

            this.fizzBuzzDataList[0].Should().Be("1");
        }

        /// <summary>
        /// GetFizzBuzzData method return Fizz Buzz if divisible by three and five
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnFizzBuzz_WhenNumberIsDivisibleByThreeAndFive()
        {
            this.fizzBuzz.Setup(m => m.ModCheck(15)).Returns(true);
            this.fizzBuzz.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.buzzRule.Setup(m => m.ModCheck(15)).Returns(true);
            this.buzzRule.Setup(m => m.Print(15, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(15);

            this.fizzBuzzDataList[14].Should().BeOneOf("fizz buzz", "wizz wuzz");
        }

        /// <summary>
        /// GetFizzBuzzData method return Fizz if divisible by three
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnFizz_WhenNumberIsDivisibleByThree()
        {
            this.fizzBuzz.Setup(m => m.ModCheck(3)).Returns(true);
            this.fizzBuzz.Setup(m => m.Print(3, It.IsAny<DayOfWeek>())).Returns("fizz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(3);

            this.fizzBuzzDataList[2].Should().BeOneOf("fizz", "wizz");
        }

        /// <summary>
        /// GetFizzBuzzData method return Buzz if divisible by five
        /// </summary>
        [Test]
        public void GetFizzBuzzData_ShouldReturnBuzz_WhenNumberIsDivisibleByFive()
        {
            this.buzzRule.Setup(m => m.ModCheck(5)).Returns(true);
            this.buzzRule.Setup(m => m.Print(5, It.IsAny<DayOfWeek>())).Returns("buzz");

            this.fizzBuzzDataList = this.service.GetFizzBuzzData(5);

            this.fizzBuzzDataList[4].Should().BeOneOf("buzz", "wuzz");
        }
    }
}
