// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StructuremapMvc.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

[assembly: WebActivator.PreApplicationStartMethod(typeof(Aviva_FizzBuzz.App_Start.StructuremapMvc), "Start")]

namespace Aviva_FizzBuzz.App_Start
{
    using System.Web.Http;
    using System.Web.Mvc;
    using Aviva_FizzBuzz.DependencyResolution;
    using StructureMap;

    /// <summary>
    /// Structure Mapping to achieve DI.
    /// </summary>
    public static class StructuremapMvc
    {
        /// <summary>
        /// Main Method
        /// </summary>
        public static void Start()
        {
            IContainer container = IoC.Initialize();
            DependencyResolver.SetResolver(new StructureMapDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new StructureMapDependencyResolver(container);
        }
    }
}