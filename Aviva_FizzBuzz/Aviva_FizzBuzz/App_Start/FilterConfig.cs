﻿//-----------------------------------------------------------------------
// <copyright file="FilterConfig.cs" company="NA">
//     Copyright (c) NA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace Aviva_FizzBuzz
{
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Register global FilterConfig 
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// Register Global Filters
        /// </summary>
        /// <param name="filters"> Global Filter Collection </param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}