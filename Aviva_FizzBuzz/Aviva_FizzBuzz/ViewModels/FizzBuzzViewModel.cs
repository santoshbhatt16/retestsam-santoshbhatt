﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzViewModel.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.ViewModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using PagedList;

    /// <summary>
    /// View Model for validating user input and displaying output.
    /// </summary>
    public class FizzBuzzViewModel
    {
        /// <summary>
        /// Gets or sets the User input
        /// </summary>
        [Display(Name = "Enter Number")]
        [Required(ErrorMessage = "Number is mandatory")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Number must be positive")]
        [Range(1, 1000, ErrorMessage = "Please enter number between 1 to 1000")]
        public int? UserInput { get; set; }

        /// <summary>
        /// Gets or sets  DisplayData
        /// </summary>
        public IPagedList<string> DisplayData { get; set; }
    }
}