﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using Aviva_FizzBuzz.Services;
    using Aviva_FizzBuzz.Services.Contracts;
    using Aviva_FizzBuzz.ViewModels;
    using PagedList;

    /// <summary>
    /// FizzBuzz Controller class
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// To achieve DI
        /// </summary>
        private IFizzBuzzService service;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class
        /// </summary>
        /// <param name="service"> IFizzBuzzService interface object</param>
        public FizzBuzzController(IFizzBuzzService service)
        {
            this.service = service;
        }

        /// <summary>
        /// GET: /FizzBuzz
        /// </summary>
        /// <param name="fizzBuzzViewModel">Model passed from FizzBuzz View</param>
        /// <param name="page"> Page number</param>
        /// <returns>Returns Input View</returns>
        public ActionResult GetFizzBuzzList(FizzBuzzViewModel fizzBuzzViewModel, int? page = 1)
        {
            if (fizzBuzzViewModel.UserInput == null)
            {
                ModelState.Clear();
                return this.View(viewName: "FizzBuzz");
            }

            if (!ModelState.IsValid)
            {
                return this.PartialView("_DisplayData", null);
            }

            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            int pageNumber = page ?? 1;

            fizzBuzzViewModel.DisplayData = this.service.GetFizzBuzzData(fizzBuzzViewModel.UserInput).ToPagedList(pageNumber, pageSize);

            return this.PartialView("_DisplayData", fizzBuzzViewModel);
        }
    }
}
