// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IoC.cs" company="Web Advanced">
// Copyright 2012 Web Advanced (www.webadvanced.com)
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

namespace Aviva_FizzBuzz.DependencyResolution
{
    using Aviva_FizzBuzz.Services;
    using Aviva_FizzBuzz.Services.Contracts;
    using Aviva_FizzBuzz.Services.Rules;
    using StructureMap;
    using StructureMap.Graph;

    /// <summary>
    /// IOC class
    /// </summary>
    public static class IoC
    {
        /// <summary>
        /// Initializing container
        /// </summary>
        /// <returns>Return container having collection of mapping.</returns>
        public static IContainer Initialize()
        {
            ObjectFactory.Initialize(x =>
                        {
                            x.Scan(scan =>
                                    {
                                        scan.TheCallingAssembly();
                                        scan.WithDefaultConventions();
                                        scan.Assembly("Aviva_FizzBuzz.Services");
                                        scan.Assembly("Aviva_FizzBuzz.Services.Tests");
                                    });
                            x.For<IRules>().Use<FizzRule>();
                            x.For<IRules>().Use<BuzzRule>();
                            x.For<IRules>().Use<DefaultRule>();
                        });
            return ObjectFactory.Container;
        }
    }
}