﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTest.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//-----------------------------------------------------------------

namespace Aviva_FizzBuzz.WebControllers.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using Aviva_FizzBuzz.Controllers;
    using Aviva_FizzBuzz.Services.Contracts;
    using Aviva_FizzBuzz.ViewModels;
    using Aviva_FizzBuzz.WebControllers.Tests.Common;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// FizzBuzz controller test class...
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTest
    {
        /// <summary>
        /// Mocking Fizz Buzz View Model
        /// </summary>
        private FizzBuzzViewModel viewModel;

        /// <summary>
        /// Mocking Context class
        /// </summary>
        private Mock<IFizzBuzzService> service;

        /// <summary>
        /// Call on each test for setting mock service and view model class
        /// </summary>
        [SetUp]
        public void SetupTests()
        {
            this.service = new Mock<IFizzBuzzService>();
            this.viewModel = new FizzBuzzViewModel(); 
        }

        /// <summary>
        /// GetFizzBuzzList action returns FizzBuzz view name when Fizz Buzz view model is null
        /// </summary>
        [Test]
        public void GetFizzBuzzList_ShouldReturnFizzBuzzViewName_WhenFizzBuzzViewModelIsNull()
        {
            this.viewModel.UserInput = null;
            FizzBuzzController controller = new FizzBuzzController(this.service.Object);
            ViewResult result = controller.GetFizzBuzzList(this.viewModel) as ViewResult;

            result.ViewName.Should().Be("FizzBuzz");
        }

        /// <summary>
        /// ViewModel throws error when user input negative number 
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnModelStateError_WhenUserInputIsNegative()
        {
            this.viewModel.UserInput = -1;
            string expectedErrorMessage = "Number must be positive";

            var result = TestModelHelper.Validate(this.viewModel);
            result.Any(r => r.ErrorMessage == expectedErrorMessage).Should().BeTrue();
        }

        /// <summary>
        /// Valid ViewState when user input positive number 
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnValidModelState_WhenUserInputIsPositive()
        {
            this.viewModel.UserInput = 1;
            string expectedErrorMessage = string.Empty;

            var result = TestModelHelper.Validate(this.viewModel);
            result.Should().BeEmpty();
        }

        /// <summary>
        /// ViewModel throws error when user input value out of range 
        /// </summary>
        [Test]
        public void FizzBuzzDataViewModel_ShouldReturnModelStateError_WhenUserInputValueOutOfRange()
        {
            this.viewModel.UserInput = 1001;
            string expectedErrorMessage = "Please enter number between 1 to 1000";
            var result = TestModelHelper.Validate(this.viewModel);

            result[0].ErrorMessage.Should().Be(expectedErrorMessage);
        }

        /// <summary>
        /// Ajax calls partial view
        /// </summary>
        [Test]
        public void GetFizzBuzzList_ShouldReturnDisplayDataPartialView_WhenFizzBuzzViewModelIsValid()
        {
            this.viewModel.UserInput = 1;
            this.service.Setup(m => m.GetFizzBuzzData(It.IsAny<int>())).Returns(this.ReturnsMockFizzBuzzData(DayOfWeek.Monday));
            var obj = new FizzBuzzController(this.service.Object);

            PartialViewResult result = (PartialViewResult)obj.GetFizzBuzzList(this.viewModel);

            result.ViewName.Should().Be("_DisplayData");
        }

        /// <summary>
        /// Model return count based on pagination size 
        /// </summary>
        [Test]
        public void GetFizzBuzzList_ShouldReturnModelCount_PaginationSize()
        {
            this.viewModel.UserInput = 21;
            this.service.Setup(m => m.GetFizzBuzzData(It.IsAny<int>())).Returns(this.ReturnsMockFizzBuzzData(DayOfWeek.Monday));
            var obj = new FizzBuzzController(this.service.Object);

            PartialViewResult actionResult = (PartialViewResult)obj.GetFizzBuzzList(this.viewModel);
            var model = actionResult.Model as FizzBuzzViewModel;

            model.DisplayData.Count.Should().Be(20);
        }

        /// <summary>
        /// Validating conditional value
        /// </summary>
        /// <param name="dayOfWeek"> Input day of the week </param>
        /// <returns>Mock list of Message display </returns>
        public List<string> ReturnsMockFizzBuzzData(DayOfWeek dayOfWeek)
        {
            string fizz = dayOfWeek == DayOfWeek.Wednesday ? "wizz" : "fizz";
            string buzz = dayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz";

            List<string> objLstDisplayMessage = new List<string>();
            objLstDisplayMessage.Add("1");
            objLstDisplayMessage.Add("2");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("4");
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("7");
            objLstDisplayMessage.Add("8");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add("11");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("13");
            objLstDisplayMessage.Add("14");
            objLstDisplayMessage.Add(fizz + " " + buzz);
            objLstDisplayMessage.Add("16");
            objLstDisplayMessage.Add("17");
            objLstDisplayMessage.Add(fizz);
            objLstDisplayMessage.Add("19");
            objLstDisplayMessage.Add(buzz);
            objLstDisplayMessage.Add(fizz);
            return objLstDisplayMessage;
        }
    }
}
