﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzService.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Aviva_FizzBuzz.Services.Contracts;
    using Aviva_FizzBuzz.Services.Rules;

    /// <summary>
    /// Base FizzBuzz logic class
    /// </summary>
    public class FizzBuzzService : IFizzBuzzService
    {
        /// <summary>
        /// Container object creation..
        /// </summary>
        private IList<IRules> rules;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzService"/> class 
        /// </summary>        
        /// <param name="rules">Return IDivisibleRule list of concrete classes</param>
        public FizzBuzzService(IList<IRules> rules)
        {
            this.rules = rules;
        }

        /// <summary>
        /// Get computed string
        /// </summary>
        /// <param name="values"> controller class will pass value </param>
        /// <returns> return list of string based on condition </returns>
        public List<string> GetFizzBuzzData(int? values)
        {
            string fizzBuzzData = null;

            List<string> listFizzBuzzData = new List<string>();

            for (int value = 1; value <= values; value++)
            {
                fizzBuzzData = string.Join(" ", this.rules.Where(r => r.ModCheck(value)).Select(r => r.Print(value, DateTime.Today.DayOfWeek)));
                listFizzBuzzData.Add(fizzBuzzData);
            }

            return listFizzBuzzData;
        }
    }
}
