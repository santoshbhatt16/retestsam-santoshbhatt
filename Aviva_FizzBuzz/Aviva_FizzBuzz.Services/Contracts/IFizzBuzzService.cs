﻿//-----------------------------------------------------------------------
// <copyright file="IFizzBuzzService.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Interface IContext declaration
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        /// Get value from the fizz buzz condition
        /// </summary>
        /// <param name="value"> Input value</param>
        /// <returns>return string </returns>
        List<string> GetFizzBuzzData(int? value);
    }
}
