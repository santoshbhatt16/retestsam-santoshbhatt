﻿//-----------------------------------------------------------------------
// <copyright file="IRules.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Contracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Fizz Buzz interface 
    /// </summary>
    public interface IRules
    {
        /// <summary>
        /// Modulo check perform here
        /// </summary>
        /// <param name="value">Input integer</param>
        /// <returns>return boolean based on rule</returns>
        bool ModCheck(int value);

        /// <summary>
        /// Value get printed here
        /// </summary>
        /// <param name="value">Input integer</param>
        /// <param name="dayOfWeek"> day of week </param>
        /// <returns>return string</returns>
        string Print(int value, DayOfWeek dayOfWeek);
    }
}
