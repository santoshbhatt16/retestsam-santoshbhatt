﻿//-----------------------------------------------------------------------
// <copyright file="DefaultRule.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Aviva_FizzBuzz.Services.Contracts;

    /// <summary>
    /// Returns Integer Value
    /// </summary>
    public class DefaultRule : IRules
    {
        /// <summary>
        /// Check value is not divisible by three and five
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <returns>Returns true if not divisible by three and five</returns>
        public virtual bool ModCheck(int value)
        {
            return value % 3 != 0 && value % 5 != 0;
        }

        /// <summary>
        /// Print integer value  
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <param name="dayOfWeek"> day of week </param>
        /// <returns>return value to string </returns>
        public virtual string Print(int value, DayOfWeek dayOfWeek)
        {
            return value.ToString();
        }
    }
}
