﻿//-----------------------------------------------------------------------
// <copyright file="BuzzRule.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Aviva_FizzBuzz.Services.Contracts;

    /// <summary>
    /// Returns Buzz Text
    /// </summary>
    public class BuzzRule : IRules
    {
        /// <summary>
        /// Check value is divisible by five
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <returns>Returns true if divisible by five</returns>
        public virtual bool ModCheck(int value)
        {
            return value % 5 == 0;
        }

        /// <summary>
        /// Print buzz  
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <param name="dayOfWeek"> day of week </param>
        /// <returns>return buzz string </returns>
        public virtual string Print(int value, DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday ? "wuzz" : "buzz";
        }
    }
}
