﻿// <copyright file="FizzRule.cs" company="NA">
//     Copyright  (c) NA. All rights reserved.
// </copyright>
//--------------------------------------------------------------------

namespace Aviva_FizzBuzz.Services.Rules
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Aviva_FizzBuzz.Services.Contracts;

    /// <summary>
    /// Modules by three condition
    /// </summary>
    public class FizzRule : IRules
    {
        /// <summary>
        /// Check value is divisible by three
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <returns>Returns true if divisible by three</returns>
        public virtual bool ModCheck(int value)
        {
            return value % 3 == 0;
        }

        /// <summary>
        /// Print fizz  
        /// </summary>
        /// <param name="value">Integer input</param>
        /// <param name="dayOfWeek"> day of week </param>
        /// <returns>return fizz string </returns>
        public virtual string Print(int value, DayOfWeek dayOfWeek)
        {
            return dayOfWeek == DayOfWeek.Wednesday ? "wizz" : "fizz";
        }
    }
}
